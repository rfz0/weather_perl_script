#!/usr/bin/env perl

require POSIX;
use strict;
use warnings;
use File::stat;
use Geo::WeatherNOAA;
use List::Util qw(max);
use Term::Cap;
use Weather::Underground;

sub term_get_cap {
	my $termios = new POSIX::Termios;
	$termios->getattr;
	my $ospeed = $termios->getospeed;
	my $terminal = Tgetent Term::Cap { TERM => undef, OSPEED =>
		$ospeed };
	return map { $terminal->Tputs($_,1) } @_;
}

sub hash_max_key_len {
	return max(map length, keys %{$_[0]});
}

sub print_indented_and_wrapped {
	my ($indent, $width, $str) = @_;
	my $len = $width;
	foreach (split(" ", $str)) {
		if (length($_) + 1 > $len) {
			printf "\n%*s ", $indent + length($_), $_;
			$len = $width - (length($_) + 1);
		} else {
			printf "%s ", $_;
			$len -= length($_) + 1;
		}
	}
	print "\n";
}

sub hash_print {
	my ($indent, $width, $sep, $hashref) = @_;
	my ($norm, $bold) = &term_get_cap(qw/me md/);	# me=bold md=normal
	while (my ($key, $val) = each %{$hashref}) {
		printf "${bold}%*s${norm}$sep", $indent, $key;
		&print_indented_and_wrapped($indent + length($sep),
			$width - ($indent + length($sep)), $val);
	}
}

sub file_expired {
	my ($cache_file, $cache_age) = @_;
	my $stat = stat($cache_file);
	my $mtime = scalar $stat->mtime;
	my $time = time;
	return (($mtime < $time) && (($time - $mtime) > $cache_age));
}

sub weather_current {
	my ($city, $state, $cache_file, $cache_age) = @_;
	my $w = Weather::Underground->new(
		place => $city . "," . $state,
		debug => 0,
		cache_file => $cache_file,
		cache_max_age => $cache_age,
	) or die "Error, could not create new weather object: $@\n";

	return $w->get_weather() or
		die "Error, calling get_weather() failed: $@\n";
}

sub weather_forecast {
	my ($city, $state, $cache_file, $cache_age) = @_;
	my $cache_opt = (-e $cache_file && !&file_expired($cache_file,
			$cache_age)) ? 'usefile' : 'save';
	return process_city_zone($city, $state, $cache_file, $cache_opt);
}

if ($#ARGV+1 != 4) {
	print "usage: $0 [city] [state abbrev] [cache dir] [cache age]\n";
	exit 2;
}
my ($city, $state, $cache_dir, $cache_age) = @ARGV;
my $current = &weather_current($city, $state, $cache_dir .
	"/weather_current_" . $city . ".dat", $cache_age);
my ($date, $warnings, $forecast, $coverage) = &weather_forecast($city,
	$state, $cache_dir . "/weather_forecast_" . $city . ".dat",
	$cache_age);
my %hash = (%{@$current[0]}, %{$forecast});
my $indent = &hash_max_key_len(\%hash);
my $width = 72;
my $sep = " : ";
my ($norm, $under) = &term_get_cap(qw/me us/);
if ($current && %{@$current[0]}) {
	print "${under}Current Conditions:${norm}\n";
	&hash_print($indent, $width, $sep, @$current[0]);
}
if (%{$forecast}) {
	print "${under}Forecast Conditions:${norm}\n";
	&hash_print($indent, $width, $sep, $forecast);
}
if (@$warnings) {
	print "${under}Warnings:${norm}\n";
	print "@$warnings\n";
}
