usage: ./weather.pl [city] [state abbrev] [cache dir] [cache age]  
  
Example:  
  
weather.pl honolulu hi ~/tmp 7200  
  
  
<pre>
Current Conditions:
              moonrise : 3:29 AM HST
      visibility_miles : 10.0
                 place : Manoa-Woodlawn, Honolulu, Hawaii
               updated : 3:22 PM HST on August 03, 2013
            conditions : Scattered Clouds
            fahrenheit : 82.8
             moonphase : Waning Crescent
                sunset : 7:08 PM HST
               sunrise : 6:05 AM HST
   temperature_celsius : 28.2
     wind_milesperhour : 3.6
   dewpoint_fahrenheit : 63
              humidity : 51
              pressure : 29.98 in 1015 hPa (Falling)
        wind_direction : NE
      dewpoint_celsius : 17
 visibility_kilometers : 16.1
               moonset : 4:56 PM HST
wind_kilometersperhour : 5.8
temperature_fahrenheit : 82.8
                clouds : (FEW) : 3200 ft 975 m Scattered Clouds (SCT) :
                         4200 ft 1280 m
               celsius : 28.2
Forecast Conditions:
                 Today : Sunny with isolated showers. Highs 82 to 88.
                         East winds 10 to 20 mph. Chance of rain 20
                         percent.
               Tonight : Partly cloudy with isolated showers. Lows 69
                         to 75. Northeast winds 10 to 15 mph. Chance of
                         rain 20 percent.
                Sunday : Sunny. Isolated showers in the morning. Highs
                         82 to 88. East winds 10 to 20 mph. Chance of
                         rain 20 percent.
          Sunday Night : Mostly clear. Isolated showers after midnight.
                         Lows 69 to 75. East winds 10 to 15 mph. Chance
                         of rain 20 percent.
                Monday : Sunny. Isolated showers in the morning. Highs
                         83 to 89. East winds 10 to 15 mph. Chance of
                         rain 20 percent.
          Monday Night : Mostly clear. Isolated showers after midnight.
                         Lows 69 to 75. East winds 10 to 15 mph. Chance
                         of rain 20 percent.
               Tuesday : Sunny. Isolated showers in the morning. Highs
                         83 to 89. East winds 10 to 15 mph. Chance of
                         rain 20 percent.
         Tuesday Night : Mostly clear. Isolated showers after midnight.
                         Lows 69 to 75. East winds 10 to 15 mph. Chance
                         of rain 20 percent.
             Wednesday : Sunny. Isolated showers in the morning. Highs
                         83 to 89. East winds 10 to 15 mph. Chance of
                         rain 20 percent.
       Wednesday Night : Mostly clear. Isolated showers after midnight.
                         Lows 69 to 75. East winds 10 to 15 mph. Chance
                         of rain 20 percent.
              Thursday : Sunny. Isolated showers in the morning. Highs
                         82 to 88. East winds 10 to 15 mph. Chance of
                         rain 20 percent.
        Thursday Night : Mostly clear. Isolated showers after midnight.
                         Lows 69 to 75. East winds 10 to 15 mph. Chance
                         of rain 20 percent.
                Friday : Sunny. Isolated showers in the morning. Highs
                         82 to 88. East winds 10 to 15 mph. Chance of
                         rain 20 percent.
</pre>
